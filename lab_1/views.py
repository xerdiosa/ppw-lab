from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Muhammad Aulia Akbar'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2001,8,27)
npm = 1806133805
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
